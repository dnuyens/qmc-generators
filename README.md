# QMC generators, aka, "The Magic Point Shop"

This repository contains point generators for quasi-Monte Carlo (QMC) point
sets in Python2/3, Matlab/Octave and C++, together with generating matrices and
generating vectors for digital sequences in base 2 and lattice sequences in
base 2.

Such QMC point sets are used to approximate high-dimensional integrals over the
$s$-dimensional unit-cube

    \[
      I(f) := \int_{[0,1]^s} f(x_1,\ldots,x_s) \, \mathrm{d}(x_1,\ldots,x_s)
    \]

by an equal-weight cubature formula:

    \[
      Q_n(f) := \frac{1}{n} \sum_{k=1}^n f(x^k_1,\ldots,x^k_s) ,
    \]

where the sample points are taken from the QMC point sequence.

The best place to jump start on using this code is the C++
[cpp/README.md](cpp/README.md) which explains you how to operate the command
line tools `digitalseq_b2g` and `latticeseq_b2`.
The `digitalseq_b2g` generator has recently gotten support to generate randomly
scrambled and randomly digitally shifted point sets.
Check out the "Advanced options" for the digital sequence generator here:
[cpp/README.md](cpp/README.md).

There is currently not so much documentation here (except for the C++
generators) but you can look at my old page
[https://www.cs.kuleuven.be/~dirkn/qmc-generators/](https://www.cs.kuleuven.be/~dirkn/qmc-generators/)
for some more explanation on the Matlab and Python generators if needed.

[![asciicast](https://asciinema.org/a/152353.png)](https://asciinema.org/a/152353)

## What is here?

There is generators for three programming languages:

- [C++](cpp/)
- [Python2/3](python/)
- [Matlab/Octave](matlab/)

and a collection of generating vectors (for lattice sequences) and generating
matrices (for digital sequences):

- [generating vectors](LATSEQ/)
- [generating matrices](DIGSEQ/)
    - [Niederreiter-Xing](DIGSEQ/nxmats)
    - [Sobol](DIGSEQ/sobolmats)

If you want to construct your own generating vectors and generating matrices,
then there is a software package 
[QMC4PDE](https://www.cs.kuleuven.be/~dirkn/qmc4pde/)
which has quite general assumptions
on the derivatives of your integrand function and can construct generating
vectors and matrices explicitly for this case. This is inspired by some PDE
applications.

## Downloading or cloning this repository

If you know how to work with `git` you can `clone` this repository by doing

    git clone https://dnuyens@bitbucket.org/dnuyens/qmc-generators.git

If you have a cloned version of the repository and you want to update it you
can do that by `fetch` in the repository directory:

    git fetch

Or you can just download the current version by clicking the
download link on the left. You also find the different "tagged" versions there.

## Acknowledging

This software is written and maintained by Dirk Nuyens.

Please acknowledge this software if you use it in your research by citing

- F. Y. Kuo and D. Nuyens. Application of quasi-Monte Carlo methods to elliptic
  PDEs with random diffusion coefficients -- a survey of analysis and
  implementation. Foundations of Computational Mathematics, 16(6):1631-1696,
  2016.
