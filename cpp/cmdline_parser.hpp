#ifndef UTIL_CMDLINE_PARSER_HPP
#define UTIL_CMDLINE_PARSER_HPP

/// Copyright Dirk Nuyens, 2017
//
// Simple command line parser.

#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

namespace util { namespace cmdline_parser {

    /// There are 4 command line argument types.
    enum arg_type { BOOL_A, INT_A, UNSIGNED_A, DOUBLE_A, LONG_DOUBLE_A, CHAR_A, STRING_A };

    /// Command line argument values are stored in a union of pointers to the
    /// variable from the user.
    union arg_u {
        bool*        b; // BOOL_A
        int*         i; // INT_A
        unsigned*    u; // UNSIGNED_A
        double*      d; // DOUBLE_A
        long double* ld;// LONG_DOUBLE_A
        const char*  c; // CHAR_A
        std::string* s; // STRING_A
    };

    #define ARG_T_CONSTRUCTOR(TYPE, ENUM_TYPE, P) arg_t(TYPE& default_value, std::string help_txt="", bool hide=false) \
                                                  : type(ENUM_TYPE), value{ .P = &default_value }, help_txt(help_txt), hide(hide) { }

    /// A command line argument is represented as the `arg_type`, the pointer
    /// to the user's variable through a `arg_u` union.
    struct arg_t {
        arg_type    type;
        arg_u       value;
        bool        seen = false;
        std::string help_txt;
        bool        hide = false; // don't show on displaying help list

        // We need default constructable for easy creation of map elements in args_t, eg: args["-m"] = { ... }
        arg_t(const char* default_value="", std::string help_txt="", bool hide=false)
            : type(CHAR_A), value{ .c = default_value }, help_txt(help_txt), hide(hide) { }
        // normal constructors simplified by macro:
        ARG_T_CONSTRUCTOR(bool, BOOL_A, b);
        ARG_T_CONSTRUCTOR(int, INT_A, i);
        ARG_T_CONSTRUCTOR(unsigned, UNSIGNED_A, u);
        ARG_T_CONSTRUCTOR(double, DOUBLE_A, d);
        ARG_T_CONSTRUCTOR(long double, LONG_DOUBLE_A, ld);
        ARG_T_CONSTRUCTOR(std::string, STRING_A, s);
        
        /// Print the value of this argument to an output stream.
        std::ostream& print_on(std::ostream& out, bool simple=true) const {
            if(simple) {
                switch(type) {
                    case BOOL_A:          return out << *value.b;
                    case INT_A:           return out << *value.i;
                    case UNSIGNED_A:      return out << *value.u;
                    case DOUBLE_A:        return out << *value.d;
                    case LONG_DOUBLE_A:   return out << *value.ld;
                    case CHAR_A:          return out << "'" << value.c << "'";
                    case STRING_A:        return out << "'" << *value.s << "'";
                }
                return out;
            } else {
                const char* seen_str = seen ? "" : "default:";
                switch(type) {
                    case BOOL_A:          return out << "bool:" << seen_str << *value.b;
                    case INT_A:           return out << "int:" << seen_str << *value.i;
                    case UNSIGNED_A:      return out << "unsigned:" << seen_str << *value.u;
                    case DOUBLE_A:        return out << "double:" << seen_str << *value.d;
                    case LONG_DOUBLE_A:   return out << "long double:" << seen_str << *value.ld;
                    case CHAR_A:          return out << "char*:" << seen_str << "'" << value.c << "'";
                    case STRING_A:        return out << "string:" << seen_str << "'" << *value.s << "'";
                }
                return out;
            }
        }

        /// Print the value of this argument to an output stream.
        friend std::ostream& operator<<(std::ostream& out, const arg_t& arg) {
            return arg.print_on(out);
        }

    };

    /// The list of arguments is represented by an std::map where the keys look like "-m".
    /// The map entries can be added by eg:
    ///     args_t args;
    ///     int m = 0;          // default value
    ///     args["-m"] = { m }; // args now holds a pointer to m (so m should stay in scope!)
    typedef std::map< std::string, arg_t > args_t;
    /// To provide for positional arguments we use an std::vector.
    /// Map entries can be added by eg:
    ///     pos_args_t pos_args;
    ///     pos_args.push_back( &args["-m"] ); // the -m arg is added as a possible first positional arg
    typedef std::vector< arg_t* > pos_args_t;

    /// Print the full args list to an output stream.
    std::ostream& print_args(std::ostream& out, const args_t& args, bool simple=true)
    {
        char a[] = {'\0', '\0'};
        for(auto& kv : args)
        {
            out << a << kv.first << '=';
            kv.second.print_on(out, simple);
            a[0] = ' ';
        }
        return out;
    }

    /// Print help on args.
    std::ostream& print_args_help(std::ostream& out, const args_t& args, 
            int width_1=12, int width_2=26, bool dont_show_defaults=false)
    {
        for(auto& kv : args)
        {
            if(kv.second.hide) continue;
            out << "  " << std::setw(width_1) << std::left << kv.first;
            std::stringstream ss_value;
            kv.second.print_on(ss_value, dont_show_defaults);
            out << "  " << std::setw(width_2) << ss_value.str();
            std::istringstream iss(kv.second.help_txt);
            std::string spacer("  ");
            for(std::string line; std::getline(iss, line); spacer.resize(width_1 + width_2 + 3*2, ' ')) {
                out << spacer << line << '\n';
            }
            if(kv.second.help_txt.size() == 0) out << '\n';
        }
        return out;
    }

    /// Print the full args list to an output stream.
    std::ostream& operator<<(std::ostream& out, const args_t& args)
    {
        return print_args(out, args);
    }

    /// Wrapper to print a detailed (including type and default marker if not
    /// set by user) full list of args to an output stream.
    struct args_detailed_print
    {
        const args_t& args;
        args_detailed_print(const args_t& args) : args(args) { }
    };

    /// Print detailled full args list to an output stream.
    std::ostream& operator<<(std::ostream& out, const args_detailed_print& d_args)
    {
        return print_args(out, d_args.args, false);
    }

    /// Parse a type T from a stringstream `s` into `a`.
    template <typename T>
    bool parse(std::stringstream &s, T &a)
    {
        s >> a;
        return !s.fail() && s.eof();
    }

    /// Parse a type T from a C string into `a`.
    template <typename T>
    bool parse(const char *c, T &a)
    {
        std::stringstream s(c);
        return parse(s, a);
    }

    /// Parse a type T from a C++ std::string into `a`.
    template <typename T>
    bool parse(const std::string& s, T &a)
    {
        std::stringstream ss(s);
        return parse(ss, a);
    }

    /// Parse command line arguments (from left to right).
    /// Use like
    ///     args_t args;
    ///     int m = 0; // variable where parsed argument will be stored + default value
    ///     args["-m"] = { m }; // args_t keeps a pointer to m (so m should stay in scope)
    ///     parse_args(argv+1, argv+argc, args);
    ///     // use `m` now, if you want to check if `m` has been set, use args["-m"].seen
    /// Note: currently the ForwardIterator is probably needed to be a `char* argv[]`.
    template <typename ForwardIterator>
    bool parse_args(ForwardIterator b, ForwardIterator e, args_t& args, pos_args_t& pos_args, bool allow_double_dash=true)
    {
        bool all_ok = true;
        const char* bool_txt[] = {"0", "1"};
        auto args_end = args.end();
        pos_args_t::size_type pos_args_count = 0;
        for( ; b < e; ++b)
        {
            std::string key(*b);
            arg_t* r_ptr = nullptr;
            std::string value;
            const char* value_char_ptr = nullptr;
            bool positional = false;
            if(key.size() && (key[0] != '-')) {
                // positional argument
                if(pos_args_count == pos_args.size()) {
                    std::clog << "ERROR: missing switch sign '-' or too many positional arguments at '" << key << "'\n";
                    all_ok = false;
                    continue;
                }
                // increase positional argument count and fetch record
                positional = true;
                value = key;
next_positional: // positional arguments are optional and we cycle through the list until the type matches
                r_ptr = pos_args[pos_args_count++];
                value_char_ptr = *b;
                //std::cout << "POSITIONAL: " << value << " @" << pos_args_count << "\n";
                // for error reporting down there we want to set key to identify this positional argument:
                std::stringstream out;
                out << "@" << pos_args_count;
                key = out.str();
            } else {
                // key-value argument
                std::string::size_type i = key.find('='), key_start = 0, key_end = key.size();
                if(allow_double_dash && (key_end > 1) && (key[0] == '-') && (key[1] == '-')) key_start = 1;
                if(i != std::string::npos) key_end = i - key_start;
                key = key.substr(key_start, key_end);
                auto kv = args.find(key);
                if(kv == args_end) {
                    std::clog << "ERROR: unknown argument '" << key << "'\n";
                    all_ok = false;
                    continue;
                    //return false; // unknown argument
                }
                // fetch record
                r_ptr = &kv->second;
                // now find value
                if(i != std::string::npos) {
                    // argument behind equals sign
                    value = *b;
                    if(i+1 == value.size()) {
                        std::clog << "ERROR: argument for '" << key << "' has length zero\n";
                        all_ok = false;
                        continue;
                    }
                    value = value.substr(i+1);
                    value_char_ptr = *b+i+1;
                    // argument is possibly next string:
                } else if((b+1 != e) && (*(*(b+1)+0) != '-')) { // check if
                    // there is a next string and if it is not an other
                    // argument (starting with a dash) (for last condition in
                    // if: have at least '\0' if b+1 is still valid)
                    ++b;
                    value = *b;
                    value_char_ptr = *b;
                    // now we now there is not valid argument coming up:
                } else if(r_ptr->type == BOOL_A) { // but bool arguments can also
                    // be used as toggle switches
                    int toggle = *r_ptr->value.b ? 0 : 1; // toggle
                    value = bool_txt[toggle];
                    value_char_ptr = bool_txt[toggle];
                } else {
                    std::clog << "ERROR: expecting argument for '" << key << "'\n";
                    all_ok = false;
                    continue;
                }
            }
            // note: can't parse straight into *r_ptr->value.i because of
            // retries of positional arguments until satisfied with parsing
            // so we have some temporary variables instead of overwriting
            // previous command line choices:
            int tmp_i = 0;
            unsigned tmp_u = 0;
            bool tmp_b = false;
            double tmp_d = 0;
            long double tmp_ld = 0;
            switch(r_ptr->type) {
                case BOOL_A:
                    if(!parse(value, tmp_b)) {
                        if(positional) goto next_positional; // positional arguments can be optional
                        std::clog << "ERROR: invalid argument '" << value << "' for bool argument '" << key << "'\n";
                        all_ok = false;
                        continue;
                    }
                    *r_ptr->value.b = tmp_b;
                    break;
                case INT_A:
                    if(!parse(value, tmp_i)) {
                        if(positional) goto next_positional; // positional arguments can be optional
                        std::clog << "ERROR: invalid argument for integer argument '" << key << "'\n";
                        all_ok = false;
                        continue;
                    }
                    *r_ptr->value.i = tmp_i;
                    break;
                case UNSIGNED_A:
                    if(!parse(value, tmp_u)) {
                        if(positional) goto next_positional; // positional arguments can be optional
                        std::clog << "ERROR: invalid argument for unsigned argument '" << key << "'\n";
                        all_ok = false;
                        continue;
                    }
                    *r_ptr->value.u = tmp_u;
                    break;
                case DOUBLE_A:
                    if(!parse(value, tmp_d)) {
                        if(positional) goto next_positional; // positional arguments can be optional
                        std::clog << "ERROR: invalid argument for double argument '" << key << "'\n";
                        all_ok = false;
                        continue;
                    }
                    *r_ptr->value.d = tmp_d;
                    break;
                case LONG_DOUBLE_A:
                    if(!parse(value, tmp_ld)) {
                        if(positional) goto next_positional; // positional arguments can be optional
                        std::clog << "ERROR: invalid argument for double argument '" << key << "'\n";
                        all_ok = false;
                        continue;
                    }
                    *r_ptr->value.ld = tmp_ld;
                    break;
                case CHAR_A:
                    r_ptr->value.c = value_char_ptr;
                    break;
                case STRING_A:
                    *r_ptr->value.s = value;
                    break;
            }
            r_ptr->seen = true;
        }
        return all_ok;
    }

    template <typename ForwardIterator>
    bool parse_args(ForwardIterator b, ForwardIterator e, args_t& args, bool allow_double_dash=true)
    {
        pos_args_t pos_args;
        return parse_args(b, e, args, pos_args, allow_double_dash);
    }

} }

#endif
