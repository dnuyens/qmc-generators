#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>

////
// (C) Dirk Nuyens, KU Leuven, 2016,2017,2018,...

#include "latticeseq_b2.hpp"
#include "cmdline_parser.hpp"

/// Print a vector to a stream, delimited by spaces.
template <typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<T>& v)
{
    int w = static_cast<int>(s.width());
    for(const auto& e : v) s << std::setw(w) << e << ' ';
    return s;
}

int main(int argc, char* argv[])
{
    typedef std::uint64_t uint_t;
    typedef long double float_t;

    unsigned m = 0, s = 0;
    std::string filename;
    bool show_help = false;
    // bind command line arguments to variables:
    util::cmdline_parser::args_t args;
    args["-s"] = { s, "number of dimensions" };
    args["-m"] = { m, "generate 2^m points"  };
    args["-f"] = { filename, "load generating vector from file" };
    args["-h"] = { show_help, "show this help message" };
    util::cmdline_parser::pos_args_t pos_args;
    pos_args.push_back( &args["-s"] );
    pos_args.push_back( &args["-m"] );
    pos_args.push_back( &args["-f"] );
    // parse the command line:
    bool ok = util::cmdline_parser::parse_args(argv+1, argv+argc, args, pos_args);

    // print help message if asked or if command line was not ok
    if(show_help || !ok)
    {
        if(!ok) std::cerr << "\n";
        std::cerr << "Generate points from a lattice sequence.\n"
                     "(C) Dirk Nuyens, 2017.\n\n"
                     "Short synopsis: " << argv[0] << " s m [filename] [< z]\n"
                     "  s             generate first s-dimensions,\n"
                     "                defaults to full dimensionality\n"
                     "  m             generate 2^m points of this sequence,\n"
                     "                defaults to twice number of bits needed\n"
                     "                to represent elements of the generating\n"
                     "                vector\n"
                     "  [filename]    generating vector read from file\n"
                     "  [< z]         generating vector read from stdin\n"
                     "When s is not given the number of dimensions is determined from the\n"
                     "input stream, the same for m.\n\n"
                     "Full list of arguments:\n";
        util::cmdline_parser::print_args_help(std::cerr, args);
        std::cerr << "\nCommand line seen: " << util::cmdline_parser::args_detailed_print(args) << "\n";
        if(!ok) std::cerr << "\nERROR parsing command line, please check messages at the top...\n";
        return 1;
    }

    // load generating vector from stdin or from file
    std::vector<uint_t> z;
    std::cerr << "s=" << s << " m=" << m << " filename=";
    std::istream* isptr(&std::cin); // use stdin if no filename was provided
    std::ifstream ifs;
    if(filename.size()) {
        ifs.open(filename);
        if(!ifs.is_open()) {
            std::cerr << " :: Error: couldn't open " << filename << "\n";
            return 1;
        }
        isptr = &ifs;
    } else {
        filename = "<stdin>";
    }
    std::cerr << filename << std::endl;
    bool print_info_again = (s == 0) | (m == 0);

    // construct generator
    qmc::latticeseq_b2<float_t, uint_t> g(s, m, *isptr);

    if(print_info_again)
        std::cerr << "=> s=" << s << " m=" << m << " filename=" << filename << "\n";

    // generate the points and print them to stdout
    int prec = std::numeric_limits<float_t>::max_digits10;
    std::cout.precision(prec);

    for(; g != g.end(); ++g)
        std::cout << std::setw(prec+4) << std::left << *g << "\n";

    return 0;
}
