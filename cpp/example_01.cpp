#include <iostream>
#include <iomanip>
#include <cassert>

#include "latticeseq_b2.hpp"

double testfunction(const std::vector<double>& x)
{
    double result = 1;
    for(unsigned j = 0; j < x.size(); ++j)
        result *= 1 + (x[j] - 0.5) / ((j+1)*(j+1));
    return result;
}

int main()
{
    // construct the lattice sequence (double precision is the default):
    int s = 200; // this is the maximum for this lattice sequence
    int m = 20;  // this is the maximum adviced for this lattice sequence
    auto g = qmc::CKN2016_latticeseq(s, m);
    // - the maximum number of dimensions is available in `g.s`
    // - the maximum "level", i.e., power of 2, is available in `g.m`
    // - the maximum number of points is $2^m$ and available in `g.n`

    double sum = 0;
    std::cout.precision(16);
    std::cout << "Using lattice sequence to approximate integral in " << g.s << " dimensions\n";
    std::cout << "   m    Q\n";
    for(int v = 0; v <= m; ++v) {
        for(int k = (1 << v) / 2; k < (1 << v); ++k, ++g)
            sum += testfunction(g.x);
        // print
        std::cout << std::setw(4) << std::right << v
                  << "\t" << std::setw(20) << std::left << (sum / (1 << v)) << "\n";
    }

    return 0;
}
