#include <iostream>
#include <iomanip>

////
// (C) Dirk Nuyens, KU Leuven, 2017,2024,...

#include "latticeseq_b2.hpp"
#include "digitalseq_b2g.hpp"

namespace qmc_debug {
    // Convenience function to test loading from file by passing in a string.
    template <typename UINT_T=std::uint64_t>
    std::vector<UINT_T>
    load_generating_matrices(const std::string& str, unsigned& s, unsigned& m, unsigned skipdims=0)
    // throw(qmc::generating_matrices_range_error)
    {
        std::istringstream is(str);
        return qmc::load_generating_matrices<UINT_T>(is, s, m, skipdims);
    }
}

void test_CKN2016()
{
    std::vector<double> expected {
        0     , 0     , 0     ,
        0.5   , 0.5   , 0.5   ,
        0.25  , 0.75  , 0.75  ,
        0.75  , 0.25  , 0.25  ,
        0.125 , 0.375 , 0.375 ,
        0.625 , 0.875 , 0.875 ,
        0.375 , 0.125 , 0.125 ,
        0.875 , 0.625 , 0.625 ,
        0.0625, 0.6875, 0.1875,
        0.5625, 0.1875, 0.6875,
        0.3125, 0.4375, 0.9375,
        0.8125, 0.9375, 0.4375,
        0.1875, 0.0625, 0.5625,
        0.6875, 0.5625, 0.0625,
        0.4375, 0.8125, 0.3125,
        0.9375, 0.3125, 0.8125,
    };
    std::vector<double> result;
    auto g = qmc::CKN2016_latticeseq(3, 4);
    for(; g != g.end(); ++g)
        for(auto x: *g) result.push_back(x);
    assert(expected == result);
    std::cout << "OK (" << __func__ << ")\n";
}

void test_JK2008_sobol()
{
    std::vector<double> expected {
        0     , 0     , 0     ,
        0.5   , 0.5   , 0.5   ,
        0.75  , 0.25  , 0.25  ,
        0.25  , 0.75  , 0.75  ,
        0.375 , 0.375 , 0.625 ,
        0.875 , 0.875 , 0.125 ,
        0.625 , 0.125 , 0.875 ,
        0.125 , 0.625 , 0.375 ,
        0.1875, 0.3125, 0.9375,
        0.6875, 0.8125, 0.4375,
        0.9375, 0.0625, 0.6875,
        0.4375, 0.5625, 0.1875,
        0.3125, 0.1875, 0.3125,
        0.8125, 0.6875, 0.8125,
        0.5625, 0.4375, 0.0625,
        0.0625, 0.9375, 0.5625,
    };
    std::vector<double> result;
    auto g = qmc::JK2008_sobolseq(3, 4);
    for(; g != g.end(); ++g)
        for(auto x: *g) result.push_back(x);
    assert(expected == result);
    std::cout << "OK (" << __func__ << ")\n";
}

void test_scrambling_operations()
{
    unsigned s = 3;
    unsigned m = 2;
    unsigned t = 2*m;
    //unsigned t2 = 2*t; // bit depth in scramble matrices
    typedef uint32_t T;
    std::vector<T> M { 167, 58, 228, 152,   // s matrices of t2-by-t
                       205, 174, 132, 56,
                       205, 118, 68, 248 };
    std::vector<T> C = { 9, 18,             // s matrices of t-by-m
                         9, 27,
                         9, 27 };
    std::vector<T> expected = { 63,  58,      // s matrices of t2-by-m
                                245, 91,
                                53,  67 };
    std::vector<T> Cout(expected.size());
    qmc::scramble(s, t, m, M.begin(), C.begin(), Cout.begin());
    assert(expected == Cout);
    std::cout << "OK (" << __func__ << ")\n";
/*
dim 0
 1  0  0  0  *  1  0  =  1  0
 1  1  0  0     0  1     1  1
 1  0  1  0     0  0     1  0
 0  1  0  1     1  0     1  1
 0  1  0  1              1  1
 1  1  1  0              1  1
 0  0  1  0              0  0
 1  0  1  1              0  0
:167 58 228 152 * 9 18 = 63 58
dim 1
 1  0  0  0  *  1  1  =  1  1
 0  1  0  0     0  1     0  1
 1  1  1  0     0  0     1  0
 1  1  0  1     1  1     0  1
 0  0  0  1              1  1
 0  1  0  1              1  0
 1  0  0  0              1  1
 1  1  1  0              1  0
:205 174 132 56 * 9 27 = 245 91
dim 2
 1  0  0  0  *  1  1  =  1  1
 0  1  0  0     0  1     0  1
 1  1  1  0     0  0     1  0
 1  0  0  1     1  1     0  0
 0  1  0  1              1  0
 0  1  0  1              1  0
 1  1  1  1              0  1
 1  0  0  1              0  0
:205 118 68 248 * 9 27 = 53 67
 */
}

void test_load_generating_matrices()
{
    const char* mini_sobol = "1 2 4 8\n1 3 5 15\n1 3 6 9\n1 3 4 10\n1 2 4 13";
    std::vector<std::uint64_t> expected { 1, 2, 4, 8, 1, 3, 5, 15, 1, 3, 6, 9, 1, 3, 4, 10, 1, 2, 4, 13 };
    unsigned s = 0, m = 0;
    auto Cs = qmc_debug::load_generating_matrices(mini_sobol, s, m);
    assert(Cs == expected);
    assert(s == 5);
    assert(m == 4);
    std::cout << "OK (" << __func__ << ")\n";
}

void test_load_generating_matrices_skipdims()
{
    const char* mini_sobol = "1 2 4 8\n1 3 5 15\n1 3 6 9\n1 3 4 10\n1 2 4 13";
    std::vector<std::uint64_t> expected { 1, 3, 5, 15, 1, 3, 6, 9, 1, 3, 4, 10, 1, 2, 4, 13 };
    unsigned s = 0, m = 0;
    auto Cs = qmc_debug::load_generating_matrices(mini_sobol, s, m, 1);
    assert(Cs == expected);
    assert(s == 4);
    assert(m == 4);
    std::cout << "OK (" << __func__ << ")\n";
}

void test_load_generating_matrices_throw()
{
    const char* mini_sobol = "1 2 4 8\n1 3 5 15\n1 3 6 9\n1 3 4 10\n1 2 4 13";
    std::vector<std::uint64_t> expected { 1, 2, 4, 8, 1, 3, 5, 15, 1, 3, 6, 9, 1, 3, 4, 10, 1, 2, 4, 13 };
    unsigned s_correct = 5, s_toobig = 6, m = 0, m_toobig = 5;
    // next call should not throw
    auto Cs_correct = qmc_debug::load_generating_matrices(mini_sobol, s_correct, m);
    assert(s_correct == 5);
    assert(m == 4);
    int catches = 0;
    try {
        // next call should throw
        auto Cs_faulty = qmc_debug::load_generating_matrices(mini_sobol, s_toobig, m);
        assert(false); // should not reach here
    } catch(qmc::generating_matrices_range_error e) {
        // we should end up here
        ++catches;
    }
    assert(s_correct == 5); // just checking this is still right
    try {
        // next call should throw
        auto Cs_faulty = qmc_debug::load_generating_matrices(mini_sobol, s_correct, m_toobig);
        assert(false); // should not reach here
    } catch(qmc::generating_matrices_range_error e) {
        // we should end up here
        ++catches;
    }
    try {
        // next call should throw
        unsigned s = 0, m = 0;
        qmc_debug::load_generating_matrices("", s, m);
        std::cout << s << " " << m << "\n";
        assert(false); // should not reach here
    } catch(qmc::generating_matrices_range_error e) {
        // we should end up here
        ++catches;
    }
    // the following should work
    {
        unsigned s = 3, m = 0;
        auto Cs = qmc_debug::load_generating_matrices(mini_sobol, s, m, 0);
        assert(s == 3);
        assert(m == 4);
    }
    try {
        // next call should throw
        unsigned s = 4, m = 0;
        auto Cs = qmc_debug::load_generating_matrices(mini_sobol, s, m, 2);
        assert(false); // sdhould not reach here
    } catch(qmc::generating_matrices_range_error e) {
        // we should end up here
        ++catches;
    }
    assert(catches == 4);
    std::cout << "OK (" << __func__ << ")\n";
}

void test_bitdepth()
{
    {
        std::vector<std::uint32_t> a = { 0 };
        assert(qmc::bit_depth(a.begin(), a.end()) == 0);
    }
    {
        std::vector<std::uint32_t> b = { 0, 1, 1, 0, 0, 1 };
        assert(qmc::bit_depth(b.begin(), b.end()) == 1);
    }
    {
        std::vector<std::uint32_t> c = { 0, 8, 1, 0, 0, 1 };
        assert(qmc::bit_depth(c.begin(), c.end()) == 4);
    }
    {
        std::vector<std::uint8_t> d = { 0, 0, 0, 0 };
        assert(qmc::bit_depth(d.begin(), d.end()) == 0);
    }
    {
        std::vector<std::uint8_t> d = { 0, 1 << 0, 1, 0, 0, 1 };
        assert(qmc::bit_depth(d.begin(), d.end()) == 1);
    }
    {
        std::vector<std::uint8_t> d = { 0, 1 << 7, 1, 0, 0, 1 };
        assert(qmc::bit_depth(d.begin(), d.end()) == 8);
    }
    std::cout << "OK (" << __func__ << ")\n";
}

int main()
{
    test_CKN2016();
    test_JK2008_sobol();
    test_scrambling_operations();
    test_load_generating_matrices();
    test_load_generating_matrices_skipdims();
    test_load_generating_matrices_throw();
    test_bitdepth();
}
