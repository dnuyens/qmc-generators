#include <iostream>
#include <iomanip>
#include <cassert>
#include <fstream>

#include "digitalseq_b2g.hpp"
#include "cmdline_parser.hpp"

int main(int argc, char* argv[])
{
    ////////////////////////////////////// Some command line parsing //////////////////////////////////
    unsigned s = 0;
    unsigned m = 0;
    std::string file = "";
    bool help = false;
    //
    util::cmdline_parser::args_t args;
    args["-s"] = {s,    "number of dimensions, determined automatically when left 0"};
    args["-m"] = {m,    "generate 2^m points, generating matrices up to m columns, determined automatically when left 0"};
    args["-f"] = {file, "load matrices from this file"};
    args["-h"] = {help, "show this help"};
    // and provide positional arguments for backward compatibility:
    util::cmdline_parser::pos_args_t pos_args;
    pos_args.push_back( &args["-s"] );
    pos_args.push_back( &args["-m"] );
    pos_args.push_back( &args["-f"] );
    // parse the command line:
    bool ok = util::cmdline_parser::parse_args(argv+1, argv+argc, args, pos_args);
    if(!ok || help)
    {
        if(!ok) std::cerr << "\n";
        std::cerr << "\nDemo for Art. (C) Dirk Nuyens 2017.\n\n"
                  << "Synopsis: " << argv[0] << " [s [m]] [file]\n\n"
                  << "Full list of arguments:\n";
        util::cmdline_parser::print_args_help(std::cerr, args);
        if(!ok) std::cerr << "\nERROR parsing command line, please check messages at the top...\n";
        return 1;
    }

    ////////////////////////////////////// Now load generating matrices //////////////////////////////////
    // construct generator `seq` by loading matrices from file
    std::ifstream ifs(file);
    if(!ifs.is_open()) {
        std::cerr << "Couldn't open file '" << file << "', use -h for help.\n";
        return 1;
    }
    std::vector<std::uint64_t> Cs = qmc::load_generating_matrices(ifs, s, m);
    qmc::digitalseq_b2g<long double, std::uint64_t> seq(s, m, Cs.begin());

    std::cout << "Matrices from " << file << ", "
              << "number of dimensions s = " << s << ", "
              << "number of columns m = " << m << "\n";

    // step through points in powers of 2
    unsigned i = 0;
    for(unsigned v = 0; v <= m; ++v) // `v` is the power of 2
    {
        for(unsigned K = 1; K <= (1u << v); K += 2, ++seq, ++i) // `K` now steps through all odd numbers in the range (exception for v=0)
        {
            std::cout << std::setw(3) << i << "th point: ";

            auto x_i = seq.cur; // the current point as an integer vector of type std::uint64_t

            for(unsigned j = 0; j < s; j++) // j-th dimension
            {
                for(int k = -1; k >= -int(m); k--) // k-th bit, with -1 being the bit after the binary point, -2 the next one etc...
                {
                    int xijk = qmc::bitget(x_i[j], k); // being 1 if k-th bit of j-th dimension of i-th point is set, 0 otherwise
                    std::cout << xijk;
                }
                std::cout << " ";
            }
            std::cout << "\n";
        }
    }

    return 0;
}
