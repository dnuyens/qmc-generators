# Remarks on the Makefile for C++ programs and single-source C++ programs

IMHO there is two choices two compile single-source C++ programs using
header-only libraries without hassle. Say our program is called `foo.cpp` and
depends on `foo.hpp`, `utilities.hpp` and `morestuff.hpp`. We now describe the
two methods.

## Method 1

We use the normal procedure and list all header files as dependencies to
`foo.o`, this looks like so:

        foo.o: foo.cpp foo.hpp utilities.hpp morestuff.hpp

Implying we will compile `foo.cpp` to `foo.o` by the automatic rule to compile
C++ code to object files (and depending on if `foo.cpp` changed or if any used
header file changed); and then we will immediately link this `foo.o` into our
program `foo`, using the automatic LINK rule. However, GNU make's automatic
rule to link `foo.o` to our executable `foo` will use the C compiler and not
the C++ compiler. Often one therefore explicitly writes how to make `foo` from
`foo.o`. The easier way out is to redefine `LINK.o` (which is normally equal to
`LINK.c`) to be `LINK.cc`, i.e., use the C++ compiler to link from `foo.o` to
executable `foo`:

        LINK.o = $(LINK.cc)

Note as well that `foo.o` has no usefulness for us in this scheme as there is
only one file that needs to be compiled, and so in fact the compilation step is
adding time that could be skipped.

Method 1 is the preferred method when having multiple source file programs.
The only problem with this method is that we have forced all `.o` files to be
linked using the C++ linker / compiler. If this is a downside at all depends if
there is other languages that need to be linked and cannot be linked using the
C++ linker / compiler.

## Method 2

If one is interested in straight compilation of a single-source `.cpp` file
then this method will give another trick which also avoids the intermediate
`.o` file.

We could try to put the `.hpp` header files and the `.cpp` file as dependencies
to the executable `foo`. Unfortunately the automatic rule to link C++ code will
put all dependencies as input files to the linker / C++ compiler, which will
complain. If we put the header files as dependencies to `foo.cpp` then the
problem is that `foo.cpp` does not get updated and so `foo` will not be
recompiled if the headers change, which is our goal of this whole exercise. Of
course we can force `foo.cpp` to get an updated time stamp, which will trigger
recompilation from `foo.cpp` to `foo`. This looks like this:

        foo.cpp: foo.hpp utilities.hpp morestuff.hpp
        	touch $@ # since $? changed

There could be multiple targets if they depend on the same source files (i.e.,
multiple single-source programs using the same header-only libraries).

We can make this more general by making a pattern rule:

        %.cpp::
        	touch $@ # since $? changed
        %.cpp:: %.hpp
        	touch $@ # since $? changed

Note that we are using double colon such that this is a final rule, see the
Makefile manual. These rules make it such that if any `.cpp` file will appear
in a dependency (also in an automatic one) then it will be considered to be
updated through the above two rules.
We now list our dependencies like so:

        foo.cpp: foo.hpp utilities.hpp morestuff.hpp

This has the added advantage that if we would have a `bar.cpp` which only
depends on `bar.hpp` that we do not have to explicitly list this dependency
(because of the `%.cpp:: %.hpp` rule), and, similarly, `foo.hpp` does not need
to be mentioned above, but it might make it more clear.
