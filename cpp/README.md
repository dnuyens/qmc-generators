# QMC point generators in C++

This directory contains C++ code to generate the points from a lattice sequence
and from a digital sequence (e.g., a Sobol sequence or a polynomial lattice
sequence). These are quasi-Monte Carlo (QMC) point sets. The point generators
are header-only libraries. Two stand-alone programs demonstrate the usage and
can be used on the command line to generate points given a generating vector
for a lattice sequence or a set of generating matrices for a digital sequence.

Such QMC point sets are used to approximate high-dimensional integrals over the
$s$-dimensional unit-cube

    \[
      I(f) := \int_{[0,1]^s} f(x_1,\ldots,x_s) \, \mathrm{d}(x_1,\ldots,x_s)
    \]

by an equal-weight cubature formula:

    \[
      Q_n(f) := \frac{1}{n} \sum_{k=1}^n f(x^k_1,\ldots,x^k_s) ,
    \]

where the sample points are taken from the QMC point sequence.

Currently the documentation in this readme only contains an explanation on how
to apply these point generators in a deterministic way. Quite often one likes
to randomize the point sets a number of times in order to be able to estimate
the variance on the resulting approximation of the integral. We hope to extend
this documentation in that way soon. Alternatively you can have a look at the
[Taiwan lecture notes](https://www.cs.kuleuven.be/~dirkn/taiwan/).

These point generators also accompany a software package called
[*QMC4PDE*](https://www.cs.kuleuven.be/~dirkn/qmc4pde/) which can construct
explicit generating vectors and generating matrices for lattice rules and
polynomial lattice rules based on a quite general description of the integrand
which was inspired by some PDE applications.

[![asciicast](https://asciinema.org/a/152353.png)](https://asciinema.org/a/152353)

#### Acknowledging this software

This software is written and maintained by Dirk Nuyens.

Please acknowledge this software if you use it in your research by citing the
Kuo & Nuyens 2016 paper (see the references at the bottom of this page).

#### Downloading or cloning this repository

If you know how to work with `git` you can `clone` this repository by doing

    git clone https://dnuyens@bitbucket.org/dnuyens/qmc-generators.git

If you have a cloned version of the repository and you want to update it you
can do that by a `fetch` in the repository directory:

    git fetch

You can also download the current version by clicking the download link on the
left.

## Lattice sequence generator

The lattice sequence generator is defined in the header file
[`latticeseq_b2.hpp`](latticeseq_b2.hpp). The `b2` means it is a base-2 lattice
sequence, which means that for every power of 2 the resulting point sets forms
a full lattice.  The intermediate points are generated in a low-discrepancy
order by making use of the radical inverse function in base-2.

We first describe a command line program which can serve as an example program
and then in the following section describe how to use the lattice sequence
generator in your own code.

### Simple command line lattice sequence generator

An example command line point generator is given by
[`latticeseq_b2.cpp`](latticeseq_b2.cpp). Which takes the number of dimensions
`s`, the level `m` (for $2^m$ points), and the generating vector file as
command line arguments. The generating vector can also be read in from standard
input. The generating vector is just a list of `s` integers separated by white
space.

#### Synopsis of command line program

```bash
# Generate 3-dimensional 2^5 points with generator from the file z.txt:
$ ./latticeseq_b2 3 5 z.txt

# Generate 3-dimensional 2^5 points with generator from stdin:
$ echo "1 5 13" | ./latticeseq_b2 3 5

# Let the generator detect the number of dimensions and power of 2,
# the power of 2 is determined as one more bit necessary to represent
# the largest component of the generating vector:
$ ./latticeseq_b2 z.txt
$ echo "1 5 13" | ./latticeseq_b2

# Specify number of dimensions but let generator detect power of 2:
$ ./latticeseq_b2 2 0 z.txt
$ ./latticeseq_b2 2 z.txt
$ echo "1 5 13" | ./latticeseq_b2 2 0
$ echo "1 5 13" | ./latticeseq_b2 2

# Specify power of 2 but let generator detect number of dimensions:
$ ./latticeseq_b2 0 3 z.txt
$ echo "1 5 13" | ./latticeseq_b2 0 3
```

#### Example output of command line program

```bash
$ echo "1 5 13" | ./latticeseq_b2
```
```text
s=0 m=0 filename=<stdin>
=> s=3 m=5 filename=<stdin>
0                        0                        0
0.5                      0.5                      0.5
0.25                     0.25                     0.25
0.75                     0.75                     0.75
0.125                    0.625                    0.625
0.625                    0.125                    0.125
0.375                    0.875                    0.875
0.875                    0.375                    0.375
0.0625                   0.3125                   0.8125
0.5625                   0.8125                   0.3125
0.3125                   0.5625                   0.0625
0.8125                   0.0625                   0.5625
0.1875                   0.9375                   0.4375
0.6875                   0.4375                   0.9375
0.4375                   0.1875                   0.6875
0.9375                   0.6875                   0.1875
0.03125                  0.15625                  0.40625
0.53125                  0.65625                  0.90625
0.28125                  0.40625                  0.65625
0.78125                  0.90625                  0.15625
0.15625                  0.78125                  0.03125
0.65625                  0.28125                  0.53125
0.40625                  0.03125                  0.28125
0.90625                  0.53125                  0.78125
0.09375                  0.46875                  0.21875
0.59375                  0.96875                  0.71875
0.34375                  0.71875                  0.46875
0.84375                  0.21875                  0.96875
0.21875                  0.09375                  0.84375
0.71875                  0.59375                  0.34375
0.46875                  0.34375                  0.09375
0.96875                  0.84375                  0.59375
```

### Using the lattice sequence generator in your own code

There is a default lattice sequence generator which is the easiest to use and
is described in the first section. If you want to load in your own generating
vector you should read the second section.

#### Using the default lattice sequence generator

If you are just wanting to use the default lattice sequence generator
`CKN2016_latticeseq` with generating vector from the paper

> R. Cools, D. Nuyens and F. Y. Kuo. Constructing embedded lattice rules for
> multivariate integration. SIAM J. Sci. Comput., 28(6), 2162-2188, 2006.

then the following is a complete working example, provided in the file
[`example_01.cpp`](example_01.cpp):

```cpp
#include <iostream>
#include <iomanip>
#include <cassert>

#include "latticeseq_b2.hpp"

double testfunction(const std::vector<double>& x)
{
    double result = 1;
    for(unsigned j = 0; j < x.size(); ++j)
        result *= 1 + (x[j] - 0.5) / ((j+1)*(j+1));
    return result;
}

int main()
{
    // construct the lattice sequence (double precision is the default):
    int s = 200; // this is the maximum for this lattice sequence
    int m = 20;  // this is the maximum adviced for this lattice sequence
    auto g = qmc::CKN2016_latticeseq(s, m);
    // - the maximum number of dimensions is available in `g.s`
    // - the maximum "level", i.e., power of 2, is available in `g.m`
    // - the maximum number of points is $2^m$ and available in `g.n`

    double sum = 0;
    std::cout.precision(16);
    std::cout << "Using lattice sequence to approximate integral in " << g.s << " dimensions\n";
    std::cout << "   m    Q\n";
    for(int v = 0; v <= m; ++v) {
        for(int k = (1 << v) / 2; k < (1 << v); ++k, ++g)
            sum += testfunction(g.x);
        // print
        std::cout << std::setw(4) << std::right << v
                  << "\t" << std::setw(20) << std::left << (sum / (1 << v)) << "\n";
    }

    return 0;
}
```

(Please note there was a typo in the inner loop which has been corrected.)

The output is as follows:

```bash
$ make example_01 && ./example_01
```
```text
c++ -std=c++11 -Wall -Werror -Wextra -Ofast -DVERSION='"Sat Nov  3 11:10:30 CET 2018 [b1858e9,build]"' -I.   example_01.cpp   -o example_01
Using lattice sequence to approximate integral in 200 dimensions
   m    Q
   0	0.3590821370748061
   1	0.679541068537403
   2	0.8303243271591291
   3	0.9277347461552783
   4	0.9672386921719227
   5	0.992066468130309
   6	0.9983316977476019
   7	0.9996588132589157
   8	0.9978890276262833
   9	0.9990583180270937
  10	0.9993930098272995
  11	0.9996554075813865
  12	0.9998041701271363
  13	0.999896585374642
  14	0.9999625202607335
  15	0.999988279900462
  16	0.9999934299837937
  17	0.9999938769734199
  18	0.9999962720245106
  19	0.999998528661471
  20	0.999999702443738
```

## Digital sequence generator

The digital sequence generator is defined in the header file
[`digitalseq_b2g.hpp`](digitalseq_b2g.hpp). The `b2` means it is a base-2
digital sequence, which means that for every power of 2 the resulting point set
is a full digital net. The further suffix `g` in `b2g` means we generate the
points in gray code ordering which has computational advantages (we only need
to take one column of the generating matrices into account to generate the next
point if we now the previous point).

We first describe a command line program which serves as an example program and
then in the following section describe how to use the digital sequence
generator in your own code.

Note that *polynomial lattice rules* can be considered a *digital net* and so
the digital sequence generator can be used to produce their points. However,
they are currently not being constructed as sequences and so it only makes
sense to use the full point set; so no real *sequence* usage.

### Simple command line digital sequence generator

An example command line point generator is given by
[`digitalseq_b2g.cpp`](digitalseq_b2g.hpp). Which takes the number of
dimensions `s`, the level `m` (for $2^m$ points), and the generating matrices
file as command line arguments. The generating matrices can also be read from
standard input.

Generating matrices for a base-2 digital net are $n \times m$ matrices over
$\mathbb{Z}_2$, i.e., they are /binary matrices/. The number of columns $m$
determines how many points can be generated, being $2^m$. The number of rows
determines the number of bits which are needed in the floating point
representation of the points of the sequence. For classical nets $n = m$. For
higher order nets of order $\alpha$ the number of rows is typically $n = \alpha
m$.

The format of the generating matrices as expected by the digital sequence
generator is as follows:

1. Each generating matrix is presented as one line in the input (thus the
number of dimensions is the number of lines of the input file).
2. Each line (i.e., each generating matrix) is represented by $m$ integers
which are representing the bits of each of the columns with the least
significand bit of the integer representation in the top row of the generating
matrix. E.g. the sequence "1 2 4 8" on a line represents the 4-by-4
diagonal matrix, and the sequence "1 3 7 15" on a line represents the 4-by-4
upper triangular matrix with all ones.

These are text files, but to make clear that the generating matrices are
encoded in this /column/ format we like to give them the `.col` extension.
This format has the benefit that the generator does not care if your generating
matrices are higher order or not.

#### Synopsis of command line program

```bash
# Generate 3-dimensional 2^4 points with generator matrices from the file sobol_mini.col:
$ ./digitalseq_b2g 3 4 sobol_mini.col

# Generate 3-dimensional 2^4 points with generator from stdin:
$ echo "1 2 4 8 \n 1 3 5 15 \n 1 3 6 9" | ./digitalseq_b2g 3 4

# Let the generator detect the number of dimensions and power of 2,
# the power of 2 is determined as the number of columns of the first line
# (and all lines should have the same number of columns!):
$ ./digitalseq_b2g sobol_mini.col
$ echo "1 2 4 8 \n 1 3 5 15 \n 1 3 6 9" | ./digitalseq_b2g

# Specify number of dimensions but let generator detect power of 2:
$ ./digitalseq_b2g 2 0 sobol_mini.col
$ ./digitalseq_b2g 2 sobol_mini.col
$ echo "1 2 4 8 \n 1 3 5 15 \n 1 3 6 9" | ./digitalseq_b2g 2 0
$ echo "1 2 4 8 \n 1 3 5 15 \n 1 3 6 9" | ./digitalseq_b2g 2

# Specify power of 2 but let generator detect number of dimensions:
$ ./digitalseq_b2g 0 3 sobol_mini.col
$ echo "1 2 4 8 \n 1 3 5 15 \n 1 3 6 9" | ./digitalseq_b2g 0 3
```

#### Example output of command line program

```bash
$ echo "1 2 4 8 \n 1 3 5 15 \n 1 3 6 9" | ./digitalseq_b2g
```
```text
s=0 m=0 filename=<stdin>
=> s=3 m=4 filename=<stdin>
0                        0                        0
0.5                      0.5                      0.5
0.75                     0.25                     0.25
0.25                     0.75                     0.75
0.375                    0.375                    0.625
0.875                    0.875                    0.125
0.625                    0.125                    0.875
0.125                    0.625                    0.375
0.1875                   0.3125                   0.9375
0.6875                   0.8125                   0.4375
0.9375                   0.0625                   0.6875
0.4375                   0.5625                   0.1875
0.3125                   0.1875                   0.3125
0.8125                   0.6875                   0.8125
0.5625                   0.4375                   0.0625
0.0625                   0.9375                   0.5625
```

#### Advanced options: random scrambling and random digital shifting

The digital sequence command line program can also generate randomly scrambled
and randomly digitally shifted point sequences using the options `-scramble`
and `-shift`. You can set the seed of the random number generator by the option
`-seed`.

The full set of options are currently:
```bash
$ ./digitalseq_b2g -h
```
```text
Generate points of a digital sequence, print to stdout (diagnostics on stderr).
(C) Dirk Nuyens, 2017, Mon Nov 27 18:19:05 CET 2017 [d2cdfc4,build].

Short synopsis: ./digitalseq_b2g [s [m]] [file] [< mat]
  s             number of dimensions to generate
  m             generate 2^m points of this sequence
  [file]        generating matrices read from file
  [< mat]       read generating matrices from stdin
If m is not given m is deduced from parsing the generating matrices, similar for s.

Full list of arguments:
  -binary       bool:default:0              print points or matrices in binary
  -f            string:default:''           load matrices from file
  -h            bool:1                      show this help message
  -integer      bool:default:0              print points in binary
  -m            unsigned:default:0          generate 2^m points
  -matrices     bool:default:0              print generating matrices (decimal or binary); exits afterwards
  -nb           bool:default:0              show index of point (state) in output
  -nofloat      bool:default:0              do not print points as float
  -s            unsigned:default:0          number of dimensions
  -scramble     bool:default:0              (randomly) scramble matrices M_j; with scrambledCs_j = M_j * C_j, see -seed and -shift
  -seed         unsigned:default:5489       seed for the Mersenne Twister to generate random scrambles and shifts, see -shift and -scramble
  -shift        bool:default:0              (randomly) digitally shift points, see -seed and -scramble
  -state        unsigned:default:0          set the state of the generator to the given value
  -t2           unsigned:default:0          bit depth for the scrambling matrices (nb of rows of scrambling matrix), see -scramble
                                            if t2=0 (default) then the same bit depth as from the generating matrices is used
```

Using the `-matrices` option you can have a peak at the generating matrices,
scrambling matrices and shift that is being used.
E.g.:
```bash
$ ./digitalseq_b2g -s=4 -m=3 -f=../DIGSEQ/sobol_Cs.col -scramble -nb -binary
```
```text
s=4 m=3 filename=../DIGSEQ/sobol_Cs.col
original bit depth / nb of rows = 3, bit depth after scrambling = 3, Csr_hash = 0x48d0c5d4a214037a, shfr_hash = 0, state k = 0
    0      000                      000                      000                      000
    0      0                        0                        0                        0
    1      111                      110                      100                      110
    1      0.875                    0.75                     0.5                      0.75
    2      101                      010                      011                      010
    2      0.625                    0.25                     0.375                    0.25
    3      010                      100                      111                      100
    3      0.25                     0.5                      0.875                    0.5
    4      011                      011                      101                      101
    4      0.375                    0.375                    0.625                    0.625
    5      100                      101                      001                      011
    5      0.5                      0.625                    0.125                    0.375
    6      110                      001                      110                      111
    6      0.75                     0.125                    0.75                     0.875
    7      001                      111                      010                      001
    7      0.125                    0.875                    0.25                     0.125
```
This shows every point in the scrambled sequence in binary and in floating point format.
To see the resulting scrambled generating matrices, the scrambling matrices and
the original generating matrices we execute:
```bash
./digitalseq_b2g -s=4 -m=3 -f=../DIGSEQ/sobol_Cs.col -scramble -nb -binary -matrices
```
```text
s=4 m=3 filename=../DIGSEQ/sobol_Cs.col
original bit depth / nb of rows = 3, bit depth after scrambling = 3, Csr_hash = 0x48d0c5d4a214037a, shfr_hash = 0, state k = 0
Dimension 1: [ 7 2 4 ]
 1  0  0  =  1  0  0  *  1  0  0
 1  1  0     1  1  0     0  1  0
 1  0  1     1  0  1     0  0  1
Dimension 2: [ 3 1 7 ]
 1  1  1  =  1  0  0  *  1  1  1
 1  0  1     1  1  0     0  1  0
 0  0  1     0  0  1     0  0  1
Dimension 3: [ 1 7 2 ]
 1  1  0  =  1  0  0  *  1  1  0
 0  1  1     0  1  0     0  1  1
 0  1  0     0  1  1     0  0  1
Dimension 4: [ 3 1 4 ]
 1  1  0  =  1  0  0  *  1  1  0
 1  0  0     1  1  0     0  1  0
 0  0  1     0  0  1     0  0  1
```


### Using the digital sequence generator in your own code

There is a default digital sequence generator which is the easiest to use and
is described in the first section. If you want to load in your own generating
matrices you should read the second section.

Documentation on how to use the randomized sequences is currently still
missing, but you can have a look at the `digitalseq_b2g.cpp` code to see how
this can be done.

#### Using the default digital sequence generator

This example is almost identical to the one in the lattice sequence section, we
just include the header file for the digital sequence generator instead of the
one for the lattice sequence generator and we initialize the default digital
sequence generator `JK2008_sobolseq` instead of the default lattice sequence
generator `CKN2016_latticeseq`.

The generating matrices used by `JK2008_sobolseq` are brought into column format
from the file `new-joe-kuo-6.21201` the [Sobol website of Frances
Kuo](http://web.maths.unsw.edu.au/~fkuo/sobol/). However, since this is
automatically linked into the program we limited the number of dimensions to
250 instead of the 21201. The relevant publication is

> S. Joe and F. Y. Kuo. Constructing Sobol sequences with better
> two-dimensional projections, SIAM J. Sci. Comput., 30, 2635-2654, 2008.

The following is a complete working example, provided in the file `example_02.cpp`:

```cpp
#include <iostream>
#include <iomanip>
#include <cassert>

#include "digitalseq_b2g.hpp"

double testfunction(const std::vector<double>& x)
{
    double result = 1;
    for(unsigned j = 0; j < x.size(); ++j)
        result *= 1 + (x[j] - 0.5) / ((j+1)*(j+1));
    return result;
}

int main()
{
    // construct the digital sequence (double precision is the default):
    int s = 200;
    int m = 20;
    auto g = qmc::JK2008_sobolseq(s, m);
    // - the maximum number of dimensions is available in `g.s`
    // - the maximum "level", i.e., power of 2, is available in `g.m`
    // - the maximum number of points is $2^m$ and available in `g.n`

    double sum = 0;
    std::cout.precision(16);
    std::cout << "Using digital sequence to approximate integral in " << g.s << " dimensions\n";
    std::cout << "   m    Q\n";
    for(int v = 0; v <= m; ++v) {
        for(int k = (1 << v) / 2; k < (1 << v); ++k, ++g)
            sum += testfunction(g.x);
        // print
        std::cout << std::setw(4) << std::right << v
                  << "\t" << std::setw(20) << std::left << (sum / (1 << v)) << "\n";
    }

    return 0;
}
```

(Please note there was a typo in the inner loop which has been corrected.)

Which gives as output:

```bash
$ make example_02 && ./example_02
```
```text
c++ -std=c++11 -Wall -Werror -Wextra -Ofast -DVERSION='"Sat Nov  3 11:12:09 CET 2018 [b1858e9,build]"' -I.   example_02.cpp   -o example_02
Using digital sequence to approximate integral in 200 dimensions
   m    Q
   0	0.3590821370748061
   1	0.679541068537403
   2	0.8296647294092551
   3	0.9167420187664658
   4	0.9552671261818106
   5	0.9762649308831997
   6	0.9878287044959551
   7	0.9939366142195863
   8	0.9968324002792962
   9	0.9984092129135929
  10	0.999207851276829
  11	0.9996020146198947
  12	0.9998013616598085
  13	0.9999016530576798
  14	0.9999515798164572
  15	0.9999766374023594
  16	0.999987485151707
  17	0.9999937445482336
  18	0.9999968792369722
  19	0.9999984382126554
  20	0.9999992196235112
```

## A note on compiling the code

The code uses features from the C++11 standard and thus needs to be compiled
with `-std=c++11`. A [`Makefile`](Makefile) is provided. (The file
[Makefile.md](Makefile.md) contains some technical remarks on the Makefile
which are irrelevant if you just want to use the generators.)

If you have a recent C++ compiler it should suffice to just do

```bash
$ make
```

If you want to do some sanity check you can run

```bash
$ make check
```

which will use a default lattice sequence and a default Sobol sequence and test
if the initial 3 dimensions come out ok. This should print some `OK` lines to
the screen. If any of the test fails you will get an `abort` from an assert
message indicating the line in the source file of which test failed.

You can further try some of the examples from above.

# References

## Publications

- R. Cools, D. Nuyens and F. Y. Kuo. Constructing embedded lattice rules for
  multivariate integration. SIAM J. Sci. Comput., 28(6), 2162-2188, 2006.
- S. Joe and F. Y. Kuo. Constructing Sobol sequences with better
  two-dimensional projections, SIAM J. Sci. Comput., 30, 2635-2654, 2008.
- F. Y. Kuo and D. Nuyens. Application of quasi-Monte Carlo methods to elliptic
  PDEs with random diffusion coefficients -- a survey of analysis and
  implementation. Foundations of Computational Mathematics, 16(6):1631-1696,
  2016.

## Resources

- [QMC4PDE](https://www.cs.kuleuven.be/~dirkn/qmc4pde/)
- [Taiwan lecture notes](https://www.cs.kuleuven.be/~dirkn/taiwan/)
- [Sobol website of Frances Kuo](http://web.maths.unsw.edu.au/~fkuo/sobol/)
