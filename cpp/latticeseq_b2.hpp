#ifndef QMC_LATTICESEQ_B2_HPP
#define QMC_LATTICESEQ_B2_HPP

////
// (C) Dirk Nuyens, KU Leuven, 2016,2017,2018,...

#include <cmath>
#include <cstdint>
#include <limits>
#include <vector>
#include <stdexcept>
#include <sstream>
#include <istream>
#include <iterator>
#include <algorithm>
#include <cassert>

namespace qmc {

#ifndef QMC_BITREVERSE // want to share this with digitalseq_b2g.hpp without having a common include
#define QMC_BITREVERSE
    /** Reverse the bits in a std::uint32_t.
      *
      * @see Bit reverse code from Stanford Bit hacks page:
      * https://graphics.stanford.edu/~seander/bithacks.html#ReverseParallel
      */
    std::uint32_t bitreverse(std::uint32_t k)
    {
        std::uint32_t v = k;
        v = ((v >> 1) & 0x55555555) | ((v & 0x55555555) << 1);  // swap odd and even bits
        v = ((v >> 2) & 0x33333333) | ((v & 0x33333333) << 2);  // swap consecutive pairs
        v = ((v >> 4) & 0x0F0F0F0F) | ((v & 0x0F0F0F0F) << 4);  // swap nibbles ...
        v = ((v >> 8) & 0x00FF00FF) | ((v & 0x00FF00FF) << 8);  // swap bytes
        v = ( v >> 16             ) | ( v               << 16); // swap 2-byte long pairs
        return v;
    }
#endif

    struct generating_vector_range_error : std::range_error
    {
        unsigned s, m, s_, m_; // current values of parsing
        generating_vector_range_error(const char* what,
                unsigned s, unsigned m, unsigned s_, unsigned m_)
        : std::range_error(what), s(s), m(m), s_(s_), m_(m_)
        {
        }
    };

    /** Load generating vector form an input stream (e.g., a file).
      *
      * @param is       the input stream to read from
      * @param s        number of dimensions to read, if set to 0, set from input
      * @param m        if set to zero, determine maximum number of bits +1 to use as n=2^m
      *
      * @throws generating_vector_range_error when s is non-zero and the
      * input stream does not have enough dimensions to fulfill this request.
      */
    template <typename UINT=std::uint64_t>
    std::vector<UINT>
    load_generating_vector(std::istream& is, unsigned& s, unsigned& m)
    //throw(generating_vector_range_error)
    {
        // parse generating vector from stdin
        std::vector<UINT> z;
        std::copy(std::istream_iterator<UINT>(is),
                  std::istream_iterator<UINT>(), std::back_inserter(z));
        if(s > z.size()) throw generating_vector_range_error("load_generating_vector::"
                "s > z.size()::You want more dimensions than are present", s, m, static_cast<unsigned>(z.size()), 0);
        if(s == 0) s = static_cast<unsigned>(z.size());
        z.resize(s);
        if(m == 0) { // set power of m
            UINT mx = *std::max_element(z.begin(), z.end());
            m = std::numeric_limits<UINT>::digits - 1;
            while(m && !((UINT(1) << (m-1)) & mx)) { m--; }
            m++;
        }
        return z;
    }

    /// Base 2 lattice sequence point generator (in radical inverse ordering).
    template <typename FLOAT_T=long double, typename UINT_T=std::uint64_t>
    struct latticeseq_b2
    {
        typedef FLOAT_T float_t;
        typedef UINT_T  uint_t;

        // TODO: add static_assert for base 2 types?
        static const int uint_t_digits2
            = std::numeric_limits<uint_t>::digits;     // assuming this is base 2 digits
        static const int float_t_digits2
            = std::numeric_limits<float_t>::digits;    // assuming this is base 2 digits
        static constexpr const float_t recipd
            = 1 / float_t(uint_t(1) << (uint_t_digits2-1)) / 2; // assuming base 2 types

        std::uint32_t k;        ///< index of the next point
        unsigned m;             ///< this base-2-lattice-sequence has `n=2^m` points
        unsigned s;             ///< number of dimensions
        uint_t n;               ///< maximum number of points `n=2^m`
        std::vector<uint_t> z;  ///< generating vector
        std::vector<float_t> shift; ///< shift vector, this is added to each point of the sequence
        std::vector<float_t> x; ///< the current point converted to float, `x = (phi(k)*z + shift) mod 1`
        static constexpr FLOAT_T scale = 1 / FLOAT_T(4294967296L); // this is 1/2^32

        /**
          * Constructor.
          *
          * @param s            number of dimensions
          * @param m            number of points is 2^m
          * @param z_begin      iterator to the generating vector, the vector
          *                     is copied into this object
          *
          * The first output point of this sequence will have been calculated
          * and is immediately available as a floating point vector of type
          * std::vector<float_t> as the member variable x, or by using the
          * dereference operator on this object.
          *
          * The sequence can be brought back into the initial state by calling
          * reset(), which will calculate the first point again and set the
          * appropriate state.
          *
          * Skipping to an arbitrary point in the sequence is possible by
          * calling the set_state(std::uint32_t) method.
          *
          * Advancing to the next point in the sequence is done by the next()
          * method.
          *
          * @see x, operator*(), cur, reset(), set_state(std::uint32_t), next()
          */
        template <typename InputIterator>
        latticeseq_b2(const unsigned& s, const unsigned& m,
                      InputIterator z_begin)
        : k(0), m(m), s(s), n(uint_t(1) << m),
          z(z_begin, z_begin+s), shift(s), x(s)
        {
            assert(this->m < uint_t_digits2); // assuming the type is base 2
            reset();
        }

        /** Constructor from input stream (e.g., file).
          *
          * @param s            the number of dimensions to generate
          *                     if set to 0 then the number of available
          *                     dimensions from the input stream will be used
          * @param m            number of points 2^m to generate
          *                     if set to 0 determined by looking at max number
          *                     of bits needed +1
          * @param is           an input stream, e.g., std::ifstream("filename")
          *
          * Note that `s` and `m` are passed by reference such that if
          * they were passed in as 0 they could be read out by the caller.
          * There is another version of this constructor who takes those
          * parameters as rvalue references (but then they both need to be).
          *
          * @see The other constructor for more explanation.
          */
        latticeseq_b2(unsigned& s, unsigned& m,
                      std::istream &is)
        : latticeseq_b2(s, m, std::begin(load_generating_vector(is, s, m)))
        {
        }

        /** Constructor from input stream (e.g., file) alternative form.
          *
          * This version allows to call the constructor with two literals for
          * `s` and `m` in case of parsing from input stream.
          *
          * @see The other constructor for more explanation.
          */
        latticeseq_b2(unsigned&& s, unsigned&& m,
                      std::istream &is)
        : latticeseq_b2(s, m, std::begin(load_generating_vector(is, s, m)))
        {
        }

        /** Advance to the next point in the sequence.
          *
          * No checks are made if the sequence is already past the end.
          * @see past_end to check if all points have been generated
          */
        float_t* next()
        {
            using std::floor;
            auto phik = scale * bitreverse(k++);
            for(unsigned j = 0; j < s; ++j) {
                x[j] = phik * z[j] + shift[j];
                x[j] = x[j] - floor(x[j]);
            }
            return &x[0];
        }

        /** Advance to the next point in the sequence.
          *
          * This one is here to be able to use this class as a forward iterator.
          */
        latticeseq_b2& operator++()
        {
            next();
            return *this;
        }

        /** Return the current point as a floating point vector.
          *
          * This one is here to be able to use this class as a forward iterator.
          */
        const std::vector<float_t>& operator*() const
        {
            return x;
        }

        /// Skip to the k-th point in the sequence.
        void set_state(std::uint32_t new_k)
        {
            k = new_k;
            next();
        }

        /// Reset the sequence to the initial state of the object `k = 0`.
        void reset()
        {
            k = 1; // index of next point
            std::copy(shift.begin(), shift.end(), x.begin());
        }

        /// Check if we have passed the end of the sequence.
        bool past_end() const
        {
            return k > n;
        }

        struct latticeseq_b2_dummy_end
        {
        };

        latticeseq_b2_dummy_end end() const
        {
            return latticeseq_b2_dummy_end();
        }

        bool operator!=(const latticeseq_b2_dummy_end&) const
        {
            return !past_end();
        }

    };

    /** Some default lattice sequence with a maximum of 250 dimensions and 2^20
      * points.
      *
      * Generating vector from the following article:
      *
      *   Cools, Nuyens, Kuo. Constructing embedded lattice rules for
      *   multivariate integration. SIAM J. Sci. Comput., 28(6), 2162-2188,
      *   2006.
      *
      * The maximum number of points was set to 2^{20}, maximum number of
      * dimensions is 250 constructed for unanchored Sobolev space with order
      * dependent weights of order 2, meaning that all 2-dimensional
      * projections are taken into account explicitly (in this case all choices
      * of weights are equivalent and this is thus a generic order 2 rule).
      */
    template <typename FLOAT_T=double, typename UINT_T=std::uint32_t>
    latticeseq_b2<FLOAT_T, UINT_T> CKN2016_latticeseq(unsigned s=250, unsigned m=20)
    {
        UINT_T z[] = {
1,
182667,
469891,
498753,
110745,
446247,
250185,
118627,
245333,
283199,
408519,
391023,
246327,
126539,
399185,
461527,
300343,
69681,
516695,
436179,
106383,
238523,
413283,
70841,
47719,
300129,
113029,
123925,
410745,
211325,
17489,
511893,
40767,
186077,
519471,
255369,
101819,
243573,
66189,
152143,
503455,
113217,
132603,
463967,
297717,
157383,
224015,
502917,
36237,
94049,
170665,
79397,
123963,
223451,
323871,
303633,
98567,
318855,
494245,
477137,
177975,
64483,
26695,
88779,
94497,
239429,
381007,
110205,
339157,
73397,
407559,
181791,
442675,
301397,
32569,
147737,
189949,
138655,
350241,
63371,
511925,
515861,
434045,
383435,
249187,
492723,
479195,
84589,
99703,
239831,
269423,
182241,
61063,
130789,
143095,
471209,
139019,
172565,
487045,
304803,
45669,
380427,
19547,
425593,
337729,
237863,
428453,
291699,
238587,
110653,
196113,
465711,
141583,
224183,
266671,
169063,
317617,
68143,
291637,
263355,
427191,
200211,
365773,
254701,
368663,
248047,
209221,
279201,
323179,
80217,
122791,
316633,
118515,
14253,
129509,
410941,
402601,
511437,
10469,
366469,
463959,
442841,
54641,
44167,
19703,
209585,
69037,
33317,
433373,
55879,
245295,
10905,
468881,
128617,
417919,
45067,
442243,
359529,
51109,
290275,
168691,
212061,
217775,
405485,
313395,
256763,
152537,
326437,
332981,
406755,
423147,
412621,
362019,
279679,
169189,
107405,
251851,
5413,
316095,
247945,
422489,
2555,
282267,
121027,
369319,
204587,
445191,
337315,
322505,
388411,
102961,
506099,
399801,
254381,
452545,
309001,
147013,
507865,
32283,
320511,
264647,
417965,
227069,
341461,
466581,
386241,
494585,
201479,
151243,
481337,
68195,
75401,
58359,
448107,
459499,
9873,
365117,
350845,
181873,
7917,
436695,
43899,
348367,
423927,
437399,
385089,
21693,
268793,
49257,
250211,
125071,
341631,
310163,
94631,
108795,
21175,
142847,
383599,
71105,
65989,
446433,
177457,
107311,
295679,
442763,
40729,
322721,
420175,
430359,
480757
        };
        assert(s <= (sizeof(z)) / (sizeof(UINT_T)));
        return latticeseq_b2<FLOAT_T, UINT_T>(s, m, std::begin(z));
    }

}

#endif // QMC_LATTICESEQ_B2_HPP
