<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="author" content="Dirk Nuyens" />
    <meta name="keywords" content="Dirk Nuyens, rank-1 lattices, CBC, component-by-component, quasi-Monte Carlo, cubature, quadrature, numerical integration, fast construction, lattice rules, lattice sequence, digital net, digital sequence, Sobol, Niederreiter-Xing" />
    <meta name="description" content="The research related personal homepage of Dirk Nuyens." />
    <meta name="robots" content="all" />
    <title>Magic point shop: QMC generators (Dirk Nuyens)</title>
    <!-- to correct the unsightly Flash of Unstyled Content. http://www.bluerobot.com/web/css/fouc.asp -->
    <script type="text/javascript"></script>
    <style type="text/css" title="currentStyle" media="screen">
      @import "../style.css";
    </style>
  </head>
  <body>
    <div id="PageContainer">
      <div id="PageHeader">
        <h1><a href="../" title="To main page"><span>Dirk Nuyens' Homepage @ cs.kuleuven.be</span></a></h1>
      </div> <!-- id="PageHeader" -->
      <div id="PageContent">
        <h1>The <em>&ldquo;Magic Point Shop&rdquo;</em> of QMC point generators and generating vectors</h1>

        <p>
        This page lists both experimental and production code for applying a
        quasi-Monte Carlo method.
        Two types of QMC methods are supported:
        </p>

        <ul>
          <li>Rank 1 <b>Lattice rules</b> and <b>lattice sequences</b> (in base 2) including higher order versions.</li>
          <li><b>Digital nets</b> and <b>digital sequences</b> (in base 2) including higher order versions (aka <b>generalized nets</b>).</li>
        </ul>

        <p>This page is in tandem with the <a href="https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/">QMC4PDE</a>
        construction codes which allow to construct generating vectors and
        generating matrices for integrands which have a natural structure
        coming from PDEs with random diffusion coefficients. Please consult
        that website for further information on how we characterize such
        integrands.
        </p>

        <p>
        Several <b>generating vectors</b> can be found on this web page.
        All lattice rules and sequences were generated by my fast component-by-component constructions.
        The Matlab version of those scripts can be found on their own page:
        <a href="../fast-cbc/">fast component-by-component Matlab/Octave scripts</a>.
        </p>

        <p>
        A more modern implementation of the fast component-by-component
        algorithms, with different features, in Python2/3 can be obtained from the
        <a href="https://people.cs.kuleuven.be/~dirk.nuyens/qmc4pde/">QMC4PDE project page</a>.
        The source of those construction scripts are also available from the 
        <a href="https://bitbucket.org/dnuyens/qmc4pde">QMC4PDE git repository</a>.
        </p>

        <p>
        Similarly, the point generators from this page are also available as a
        <a href="https://bitbucket.org/dnuyens/qmc-generators">qmc-generators git repository</a>.
        There is some additional code which can be found on the git repository pages.
        Some older code is also linked from the current page.
        </p>

        <p>You can download the new QMC4PDE generators as a package. The
        current version of the <b>Magic Point Shop qmc-generators package</b> is <b>14
        Dec 2017</b>.
        </p>
        <div style="text-align: center; border: 1pt solid blue;">
          Download <a href="qmc-generators-20171214.tgz">qmc-generators-20171214.tgz</a>. <br/>
          Optional download <a href="sobolmats.tgz">Sobol matrices (including higher order) (large file 21 MB)</a>.<br/>
          Optional download <a href="nxmats.tgz">Niederreiter-Xing matrices (including higher order)</a>.<br/>
        </div>

        <p>If you use this code in your research then please cite on of the
        related papers. If you don't know which one to cite then you can
        cite the origin of the current point generators which come form the
        QMC4PDE project:</p>
        <p>
        F.Y. Kuo &amp; D. Nuyens. Application of quasi-Monte Carlo methods to elliptic PDEs with random diffusion coefficients - a survey of analysis and implementation, <i>Foundations of Computational Mathematics</i>, 16(6):1631-1696, 2016.
        (<a href="https://link.springer.com/article/10.1007/s10208-016-9329-5">springer link</a>, <a href="https://arxiv.org/abs/1606.06613">arxiv link</a>)
        </p>

        <p>
        The following is a demo demonstrating the digital sequence generator
        which is in the git repository (and also on this page):
        </p>
        <script type="text/javascript" src="https://asciinema.org/a/152353.js" id="asciicast-152353" async data-autoplay="true" data-speed="2" data-size="small"></script>

        <p>
        The vectors for the <b>Niederreiter-Xing</b> sequence have been
        obtained from
        <a href="http://www.ricam.oeaw.ac.at/people/page/pirsic/niedxing/">Pirsic's website</a>.
        The code necessary to convert and assemble the <em>generating matrices</em>
        from Pirsic's website into a suitable generating vector (consisting of
        generating matrices) is provided.  Also generating vectors for the
        <b>Sobol' sequence</b> are provided.  These have been obtained from 
        <a href="http://web.maths.unsw.edu.au/~fkuo/sobol/">Kuo's website</a>.
        </p>
        
        <p>
        The typical convergence obtained using QMC points is
        near <i>O</i>(<i>N</i><sup>&minus;1</sup>).
        In classical <i>s</i> dimensional function spaces there is a
        (log <i>N</i>)<sup><i>s</i></sup> factor, while in appropriately
        <em>weighted function spaces</em> this exponential dependency on the
        number of dimensions can be removed.
        This website is however just about software to generate QMC points, the
        warnings are in the fine print of your receipt&hellip;
        (You might consult 
        <a href="http://www.ams.org/notices/200511/fea-sloan.pdf">this article</a>
        by F. Y. Kuo and Ian H.  Sloan in <em>AMS Notices</em> to learn more
        about weighted function spaces and tractability.)
        </p>

        <p>
        If the function is sufficiently smooth then it is also possible to
        achieve <b>higher order of convergence</b> <i>O</i>(<i>N</i><sup>&minus;&alpha;</sup>.
        The code on this page is also capable of generating higher order
        lattice rules (and sequences) and higher order digital nets (and sequences).
        One only has to initialize the point generator with a higher order
        generating vector.
        The generator will then deliver the correct points.
        For lattice rules this is naturally done with the same code, while the
        digital nets and sequence code is written in such a way that higher
        order generating vectors have the correct effect.
        </p>

        <p>
        Currently all code on this page is concerned with sequences in base 2
        as this is the most efficient case to implement and as such also the
        most interesting case for practitioners.
        </p>

        <p>
        <b>NB:</b>
        If you notice any errors or inaccuracies in the code or on this page, please do not hesitate to <a href="http://cwisdb.kuleuven.be/persdb-bin/persdb?oproep=persoon&amp;lang=E&amp;fnaam=nuyens&amp;vnaam=dirk">email me</a>.
        I am also happy to answer any usage related questions you might have.
        </p>
        
        <h2>Lattice sequences in base 2</h2>

        <p>
        Given a generating vector <i><b>z</b></i> of integers, the <i>k</i>th
        point of the sequence is given by
        </p>
        <div class="float"><div class="displayformula">
            <i><b>x</b><sub>k</sub></i> := &phi;(<i>k</i>) <i><b>z</b></i> mod <i>1</i>,
            for <i>k</i>=0,1,2,&hellip;,
        </div></div>
        <p>
        where &phi; is typically the <em>radical inverse</em> or the <em>gray
        coded radical inverse</em> function in the base of the lattice sequence.
        </p>

        <p>
        The radical inverse of an integer <i>k</i> with <i>m</i> digit base
        <i>b</i> expansion
        <div class="float"><div class="displayformula">
          <i>k</i> = (<i>k<sub>m-1</sub> k<sub>m-2</sub> &#x22EF; k<sub>0</sub></i>)<sub>b</sub> 
        </div></div>
        is obtained by mirroring the digits at the fractional point, i.e., 
        <div class="float"><div class="displayformula">
          &phi;(k) = (0.<i>k<sub>0</sub> k<sub>1</sub> &#x22EF; k<sub>m-1</sub></i>)<sub>b</sub>
          .
        </div></div>
        Obviously the result is a rational in <i>b<sup>m</sup></i> and an
        alternative view is thus to look at this mapping as a permutation of the
        integers in {0,&hellip;,<i>b<sup>m</sup></i>&minus;1}.
        In other words we are reversing the digits.
        Luckily, in base 2 reversing digits can be done efficiently.
        At the end of this section there is a Matlab/Octave and a C++ version
        to reverse bits.
        To obtain the radical inverse in base 2 of an unsigned 32 bit integer
        then just requires scaling the result of the bit reversion by
        2<sup>&minus;32</sup>.
        </p>

        <p>
        Working in gray code ordering has speed advantages for digital nets
        (and sequences) but less so for lattice sequences.
        <!--Nevertheless code is provided for a gray coded radical inverse
        ordering.
        If one would be willing to use inline assembly then one can make use of
        a processor instruction to find the next changing bit in the gray code
        sequence (and as such the radical inversion can be avoided).
        This is however not possible in Matlab or Octave and the implementation
        given below is therefore slower than that of the plain vanilla radical
        inverse. 
        The Matlab code for this ordering is only provided for completeness.-->
        </p>

        <p>
        NOTE: although below are links to source code it is better to consult the 
        <a href="https://bitbucket.org/dnuyens/qmc-generators">qmc-generators git repository</a> which
        contains the same source code and additional examples and explanations.
        </p>

        <p>
        The following Matlab/Octave scripts are available (related to the QMC4PDE project):
        </p>
        <ol>
          <li><a href="matlab/latticeseq_b2.m">matlab/latticeseq_b2.m</a>: lattice sequence point generator in radical inverse order<!-- (preferred version)--></li>
          <!--li><a href="matlab/latticeseq_b2g.m">matlab/latticeseq_b2g.m</a>: lattice sequence point generator in gray coded radical inverse order (alternative version)</li-->
          <li><a href="matlab/bitreverse32.m">matlab/bitreverse32.m</a>: bit reversion of 32 bit unsigned integers</li>
        </ol>

        <!--p>
        Also the following older C++ source is available.
        (Note that this is a preliminary and simplified version of my McInt library.)
        </p>
        <ol>
          <li><a href="cpp/lattice_sequence.hpp">lattice_sequence.hpp</a>:
          this file contains the hard coded class <tt>lattice_sequence_base2_CKN</tt> which is an
          explicit implementation of the lattice sequence published in the
          Cools, Kuo and Nuyens SIAM SISC paper (2006).</li>
          <li><a href="cpp/lattice_sequence.cpp">lattice_sequence.cpp</a>: the
          generating vector from that paper</li>
          <li><a href="cpp/bit_reverse.hpp">bit_reverse.hpp</a>: a completely
          generic bit reversion implementation for unsigned int types up to 64
          bit</li>
          <li><a href="cpp/radical_inverse.hpp">radical_inverse.hpp</a>: a hard
          coded radical inverse function for 32 bit unsigned integers</li>
        </ol-->

        <p>
        The following C++ generators are available (related to the QMC4PDE project):
        </p>
        <ol>
          <li><a href="cpp/latticeseq_b2.hpp">cpp/latticeseq_b2.hpp</a>: you can use this class
            in your own code.</li>
          <li><a href="cpp/latticeseq_b2.cpp">cpp/latticeseq_b2.cpp</a>: this
            is an example command line point generator using the previous
            header file.</li>
          <li>Two example files to integrate a 100-dimensional function are also provided:
            <a href="cpp/example_01.cpp">cpp/example_01.cpp</a> and
            <a href="cpp/example_02.cpp">cpp/example_02.cpp</a>.
            These examples and how to use the point generators is well
            documented by navigating to the cpp directory of the  
            <a href="https://bitbucket.org/dnuyens/qmc-generators">qmc-generators git repository</a>.
        </ol>

        <p>
        The following Python2/3 code is available (related to the QMC4PDE project):
        </p>
         <ol>
          <li><a href="python/latticeseq_b2.py">python/latticeseq_b2.py</a>:
            this file contains a point generator class for use in your own code
            and also has a main method such that it can also be ran as a
            command line point generator.
        </ol>

        <p>
        Here is a small list of generating vectors for lattice sequences in
        base 2 in the directory <a href="LATSEQ">LATSEQ</a>.
        They were all generated by my 
        <a href="../fast-cbc/">fast component-by-component algorithms</a>.
        </p>

        <ol>
            <li><a href="LATSEQ/exod2_base2_m20_CKN.txt">exod2_base2_m20_CKN</a>:
            The 250-dimensional base-2 generating vector for order-2 weights up to 
            2<sup>20</sup> = 1048576 points from the Cools, Kuo and Nuyens SIAM
            SISC paper (2006).</li>
            <li><a href="LATSEQ/exod2_base2_m20.txt">exod2_base2_m20</a>: 
            A 600-dimensional base-2 generating vector for order-2 weights up
            to 2<sup>20</sup> = 1048576 points.</li>
            <li><a href="LATSEQ/exod2_base2_m13.txt">exod2_base2_m13</a>: 
            A 600-dimensional base-2 generating vector for order-2 weights up
            to 2<sup>13</sup> = 8192 points.</li>
            <li><a href="LATSEQ/exod8_base2_m13.txt">exod8_base2_m13</a>: 
            A 600-dimensional base-2 generating vector for order-8 weights (all
            1) up to 2<sup>13</sup> = 8192 points.</li>
            <li><a href="LATSEQ/exew_base2_m20_a3_HKKN.txt">exew_base2_m20_a3_HKKN.txt</a>:
            A 10-dimensional base-2 generating vector for an equal weight (all
            one) Korobov space with optimal rate <i>N</i><sup>-3</sup> up to
            2<sup>20</sup> = 1048576 points from Hickernell, Kritzer, Kuo and
            Nuyens (2011) - Weighted compound integration rules with higher
            order convergence for all <i>N</i>.
            </li>
        </ol>

        <p>
        For randomization one uses Cranley-Patterson shifting:
        </p>
        <ol>
            <li><a href="matlab/mod1shift.m">matlab/mod1shift.m</a></li>
        </ol>

        
        <h2>Digital sequences in base 2</h2>

        <p>
        Digital sequences are not generated by an integer generating vector as
        is the case for lattice sequences, but they are generated by a set of
        generating matrices. Here we think of them as a vector of matrices.
        These are matrices with elements from a finite field. For a digital
        sequence in base 2 this finite field is simply
        <i><b>Z</b></i><sub>2</sub>.
        </p>

        <p>
        NOTE: although below are links to source code it is better to consult the 
        <a href="https://bitbucket.org/dnuyens/qmc-generators">qmc-generators git repository</a> which
        contains the same source code and additional examples and explanations.
        </p>

        <p>
        The following Matlab/Octave scripts are available (related to the QMC4PDE project):
        </p>
        <ol>
          <li><a href="matlab/digitalseq_b2g.m">matlab/digitalseq_b2g.m</a>: digital sequence point generator in radical inverse gray code order</li>
          <li><a href="matlab/bitreverse64.m">matlab/bitreverse64.m</a>: bit reversion of 64 bit unsigned integers</li>
        </ol>

        <p>
        The following C++ generators are available (related to the QMC4PDE project):
        </p>
        <ol>
          <li><a href="cpp/digitalseq_b2g.hpp">cpp/digitalseq_b2g.hpp</a>: you can use this class
            in your own code.</li>
          <li><a href="cpp/digitalseq_b2g.cpp">cpp/digitalseq_b2g.cpp</a>: this
            is an example command line point generator using the previous
            header file.</li>
        </ol>

        <p>
        The following Python2/3 code is available (related to the QMC4PDE project):
        </p>
         <ol>
          <li><a href="python/digitalseq_b2g.py">python/digitalseq_b2g.py</a>:
            this file contains a point generator class for use in your own code
            and also has a main method such that it can also be ran as a
            command line point generator.
        </ol>

        <p>
        The following older C++ programs
        to convert and assemble generating matrices from
        <a href="http://www.ricam.oeaw.ac.at/people/page/pirsic/niedxing/">Pirsic's website</a>
        are also still available:
        </p>
        <ol>
          <li><a href="cpp/row2full.cc">row2full.cc</a>: given a matrix in the
          format from Pirsic this writes the full matrix as an array of zeros
          and ones; similar to the libseq format (which can be found in the
          archive to generate the points).</li>
          <li><a href="cpp/compact_matrices.cc">compact_matrices.cc</a>: given
          a matrix in the format of the libseq code this program will print out
          an integer for every column in a generating matrix in the format
          needed for the Matlab point generator.</li>
          <li><a href="cpp/homerge.cc">homerge.cc</a>: this program implements
          Josef Dick's explicit higher order construction based on generating
          matrices in the libseq format as outputted by the row2full
          program.</li>
        </ol>

        <p>
        There is a collection of generating matrices for digital sequences (in
        base 2) in the directory <a href="DIGSEQ">DIGSEQ</a>.
        Matrices for Niederreiter-Xing digital sequences and higher order
        sequences constructed on the basis of these matrices can be found in the directory
        <a href="DIGSEQ/nxmats">DIGSEQ/nxmats</a>.
        Similarly we provide generating matrices for the Sobol' sequence 
        corresponding to the Joe and Kuo paper in ACM Transactions om
        Mathematical Software can be found in the 
        <a href="DIGSEQ/sobolmats">DIGSEQ/sobolmats</a> directory.
        You can also download <a href="sobolmats.tgz">sobolmats.tgz</a>.
        All of these come in plain vanilla or higher order versions.
        </p>

        <p>
        For randomization one can use digital shifting:
        </p>
        <ol>
            <li><a href="matlab/digitalshift.m">matlab/digitalshift.m</a></li>
        </ol>

      </div> <!-- id="PageContent" -->
      <div id="PageFooter">
        <span id="Copyright">&copy; Copyright Dirk Nuyens.</span>
        <span id="Standards"><a href="http://validator.w3.org/check?uri=referer">XHTML</a> and <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> compliant (please mail any irregularities).</span>
        <span id="Disclaimer">Disclaimer: I've learned to speak for myself, now I learn to live with the mistakes.</span>
      </div> <!-- id="PageFooter" -->
      <script type="text/javascript">
        /* <![CDATA[ */
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-105294-3']);
        _gaq.push(['_trackPageview']);
        (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
         /* ]]> */
       </script> 
    </div> <!-- id="PageContainer" -->
  </body>
</html>
