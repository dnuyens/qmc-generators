DATE=`date "+%Y%m%d"`
cd .. # step one directory down such that the whole archive is one directory
COPYFILE_DISABLE=1 tar czf qmc-generators/qmc-generators-$DATE.tgz \
                              qmc-generators/index.html          \
                              qmc-generators/matlab/bitreverse32.m         \
                              qmc-generators/matlab/bitreverse64.m         \
                              qmc-generators/matlab/latticeseq_b2.m        \
                              qmc-generators/matlab/digitalseq_b2g.m       \
                              qmc-generators/cpp/Makefile \
                              qmc-generators/cpp/latticeseq_b2.hpp \
                              qmc-generators/cpp/latticeseq_b2.cpp \
                              qmc-generators/cpp/digitalseq_b2g.hpp \
                              qmc-generators/cpp/digitalseq_b2g.cpp \
                              qmc-generators/cpp/cmdline_parser.hpp \
                              qmc-generators/cpp/Makefile \
                              qmc-generators/cpp/example_01.cpp \
                              qmc-generators/cpp/example_02.cpp \
                              qmc-generators/cpp/sobol_mini.col \
                              qmc-generators/cpp/z.txt \
                              qmc-generators/cpp/unit_test.cpp \
                              qmc-generators/python/latticeseq_b2.py \
                              qmc-generators/python/digitalseq_b2g.py \
    qmc-generators/DIGSEQ/sobol_mini.col \
    qmc-generators/LATSEQ

