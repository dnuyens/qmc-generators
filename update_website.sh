DATE=`date "+%Y%m%d"`
rsync  --delete -avzh \
    qmc-generators-$DATE.tgz \
    index.html               \
    DIGSEQ \
    LATSEQ \
    sobolmats.tgz \
    nxmats.tgz \
 dirkn@ssh1.cs.kuleuven.be:public_html/qmc-generators/
rsync  --delete -avzh \
    matlab/bitreverse32.m         \
    matlab/bitreverse64.m         \
    matlab/latticeseq_b2.m        \
    matlab/digitalseq_b2g.m       \
 dirkn@ssh1.cs.kuleuven.be:public_html/qmc-generators/matlab/
rsync  --delete -avzh \
    cpp/Makefile \
    cpp/latticeseq_b2.hpp \
    cpp/latticeseq_b2.cpp \
    cpp/digitalseq_b2g.hpp \
    cpp/digitalseq_b2g.cpp \
    cpp/cmdline_parser.hpp \
    cpp/Makefile \
    cpp/example_01.cpp \
    cpp/example_02.cpp \
    cpp/sobol_mini.col \
    cpp/z.txt \
    cpp/unit_test.cpp \
 dirkn@ssh1.cs.kuleuven.be:public_html/qmc-generators/cpp/
rsync  --delete -avzh \
    python/latticeseq_b2.py \
    python/digitalseq_b2g.py \
 dirkn@ssh1.cs.kuleuven.be:public_html/qmc-generators/python/
